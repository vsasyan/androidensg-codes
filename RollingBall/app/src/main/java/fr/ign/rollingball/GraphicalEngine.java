package fr.ign.rollingball;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;

import androidx.annotation.NonNull;

public class GraphicalEngine implements SurfaceHolder.Callback {
    private final Ball ball;
    private SurfaceHolder surfaceHolder;
    private Thread drawingThread;
    private final Paint ballPaint;

    GraphicalEngine(Ball ball) {
        this.ball = ball;
        ballPaint = new Paint();
        ballPaint.setColor(Color.RED);
    }
    @Override
    public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) {
        this.surfaceHolder = surfaceHolder;
        drawingThread = new Thread() {
            @Override
            public void run() {
                Canvas canvas;
                while (!isInterrupted()) {
                    try {
                        canvas = GraphicalEngine.this.surfaceHolder.lockCanvas();
                        if (canvas != null) {
                            GraphicalEngine.this.draw(canvas);
                            GraphicalEngine.this.surfaceHolder.unlockCanvasAndPost(canvas);
                        }
                    } catch (NullPointerException e) {}

                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {}
                }
            }
        };
        drawingThread.start();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        ball.setMaxX(i1);
        ball.setMaxY(i2);
    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) {
        drawingThread.interrupt();
        this.surfaceHolder = null;
    }

    private void draw(Canvas canvas) {
        canvas.drawColor(Color.BLACK);
        canvas.drawCircle(ball.getX(), ball.getY(), Ball.RADIUS, ballPaint);
    }
}
