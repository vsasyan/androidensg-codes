package fr.ign.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("ENSG", "onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("ENSG", "onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("ENSG", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("ENSG", "onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("ENSG", "onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("ENSG", "onResume");
    }
}
